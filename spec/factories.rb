FactoryGirl.define do
  factory :user do
    name     "manolito"
    email    "manolito@me.com"
    password "manolito"
    password_confirmation "manolito"
  end
end