require 'spec_helper'

describe "UserPages" do

  subject { page }

  describe "Sign up page" do
    before {visit signup_path}
    it {should have_selector('h1', text: 'Sign up')}
    it { should have_selector('title', text: full_title('Sign up')) }
  end

  describe "edit" do
    let (:user) { FactoryGirl.create(:user) }
    before { visit edit_user_path(user) }

    describe "page" do
      it { should have_selector('h1', text: "Update your profile") }
      it { should have_selector('title', text: "Edit user") }
      it { should have_link('change', text: "http://gravatar.com/emails") }
    end

    describe "with invalid information" do
      before { click_button "Save changes" }
      
      it { should have_content('error') }
    end
  end
end

